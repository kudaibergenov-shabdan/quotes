import axios from "axios";

const axiosApi = axios.create({
    baseURL: 'https://quotes-9ef70-default-rtdb.firebaseio.com'
});

export default axiosApi;