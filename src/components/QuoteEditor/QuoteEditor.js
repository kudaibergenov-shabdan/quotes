import React from 'react';
import './QuoteEditor.css';

const QuoteEditor = (props) => {
    const author = props.quote ? props.quote.author : '';
    const text = props.quote ? props.quote.text : '';
    const categoryId = props.quote ? props.quote.category : '';

    return (
        <div className="QuoteEditor">
            <h3>{props.title}</h3>
            <form onSubmit={props.submitForm}>
                <div className="block category-block">
                    <span className="block__title">Category</span>
                    <select
                        name="category"
                        onChange={props.onInputChange}
                        value={categoryId}
                    >
                        {props.quoteCategories.map(category => (
                            <option key={category.id} value={category.id}>
                                {category.title}
                            </option>
                        ))}
                    </select>
                </div>
                <div className="block author-block">
                    <span className="block__title">Author</span>
                    <input
                        type="text"
                        name="author"
                        defaultValue={author}
                        onChange={props.onInputChange}
                    />
                </div>
                <div className="block quote-text-block">
                    <span className="block__title">Quote text</span>
                    <textarea
                        className="quote-text"
                        name="text"
                        defaultValue={text}
                        onChange={props.onInputChange}
                    />
                </div>
                <div className="btn-block">
                    <button className="save-btn">Save</button>
                </div>

            </form>
        </div>
    );
};

export default QuoteEditor;