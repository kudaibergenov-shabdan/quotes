import React, {useEffect, useState} from 'react';
import axiosApi from "../../axiosApi";
import QuoteEditor from "../QuoteEditor/QuoteEditor";

const EditQuote = ({quoteCategories, history, match}) => {

    const [quote, setQuote] = useState(null);
    const [categories] = useState(quoteCategories.filter(category => category.id !== 'all'));

    useEffect(() => {
        const fetchQuote = async () => {
            const response = await axiosApi.get(`quotes/${match.params.id}.json`);
            return response.data;
        };
        fetchQuote()
            .then(resolve => {
                setQuote(resolve);
            })
            .catch(err => console.error(err));

    }, [match.params.id]);

    const onInputChange = e => {
        const {name, value} = e.target;

        setQuote(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const updateQuote = async e => {
        e.preventDefault();
        try {
            if (quote.author && quote.text) {
                await axiosApi.put(`quotes/${match.params.id}.json`, quote);
                history.push('/');
            } else {
                throw new Error();
            }
        }
        catch (error) {
            alert('Please check text fields. Author or quote text is empty');
        }
    }

    return (
        <QuoteEditor
            title="Edit a quote"
            quote={quote}
            quoteCategories={categories}
            onInputChange={e => onInputChange(e)}
            submitForm={e => updateQuote(e)}
        />
    );
};

export default EditQuote;