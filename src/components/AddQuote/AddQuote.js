import React, {useState} from 'react';
import QuoteEditor from "../QuoteEditor/QuoteEditor";
import axiosApi from "../../axiosApi";

const AddQuote = ({quoteCategories, history}) => {
    const [quote, setQuote] = useState({
        author: '',
        text: '',
        category: ''
    });

    const createQuote = async e => {
        e.preventDefault();

        try {
            if (quote.category) {
                if (quote.author && quote.text) {
                    await axiosApi.post('/quotes.json', quote);
                    history.push('/');
                } else {
                    alert('Please check text fields. Author or quote is empty');
                }
            } else {
                alert('Please, choose category');
            }
        } finally {

        }
    };

    const onInputChange = e => {
        const {name, value} = e.target;
        setQuote(prev => ({
            ...prev,
            [name]: value
        }));
    };

    return (
        <QuoteEditor
            title="Submit new quote"
            quoteCategories={quoteCategories}
            onInputChange={e => onInputChange(e)}
            submitForm={e => createQuote(e)}
        />
    );
};

export default AddQuote;