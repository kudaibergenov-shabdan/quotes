import React, {useCallback, useEffect, useState} from 'react';
import './Quotes.css';
import {NavLink} from "react-router-dom";
import axiosApi from "../../axiosApi";

const Quotes = ({history, match}) => {
    const [quoteCategories] = useState([
        {title: 'All', id: 'all'},
        {title: 'Star Wars', id: 'star-wars'},
        {title: 'Motivational', id: 'motivational'},
        {title: 'Famous people', id: 'famous-people'},
        {title: 'Saying', id: 'saying'},
        {title: 'Humour', id: 'humour'}
    ]);

    const [quotes, setQuotes] = useState(null);

    const fetchQuotes = useCallback(async () => {
        let response;
        try {
            if (match.params.id === undefined || match.params.id === 'all') {
                response = await axiosApi.get('/quotes.json');
            } else {
                response = await axiosApi.get(`/quotes.json?orderBy="category"&equalTo="${match.params.id}"`);
            }
            setQuotes(response.data);
        } finally {

        }
    }, [match.params.id]);

    useEffect(() => {
        fetchQuotes()
            .catch(err => console.error(err));
    }, [fetchQuotes]);

    const dropQuote = async quoteId => {
        await axiosApi.delete(`/quotes/${quoteId}.json`);
        fetchQuotes().catch(err => console.error(err));
    }

    const updateQuote = (quoteId) => {
        history.replace(`/quotes/${quoteId}/edit`);
    }

    return (
        <div className="Quotes">
            <div className="quotes-nav">
                <nav>
                    <ul>
                        {
                            quoteCategories.map(category => (
                                <li key={category.id}>
                                    <NavLink to={"/quotes/" + category.id}>{category.title}</NavLink>
                                </li>
                            ))
                        }
                    </ul>
                </nav>
            </div>
            <div className="quotes-box">
                {quotes &&
                Object.keys(quotes).map(quoteId => (
                    <div key={quoteId} className="quote-box">
                        <p>"{quotes[quoteId].text}"</p>
                        <span>-&nbsp;{quotes[quoteId].author}</span>
                        <div className="btn-block">
                            <button className="dropQuote" onClick={() => dropQuote(quoteId)}>Delete</button>
                            <button className="updateQuote" onClick={() => updateQuote(quoteId)}>Update</button>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Quotes;