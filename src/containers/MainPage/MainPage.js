import React, {useEffect, useState} from 'react';
import './MainPage.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Header from "../Header/Header";
import Quotes from "../Quotes/Quotes";
import AddQuote from "../../components/AddQuote/AddQuote";
import EditQuote from "../../components/EditQuote/EditQuote";

const MainPage = () => {
    const [quoteCategories, setQuoteCategories] = useState(null);

    useEffect(() => {
        setQuoteCategories([
            {title: 'All', id: 'all'},
            {title: 'Star Wars', id: 'star-wars'},
            {title: 'Motivational', id: 'motivational'},
            {title: 'Famous people', id: 'famous-people'},
            {title: 'Saying', id: 'saying'},
            {title: 'Humour', id: 'humour'}
        ])
    }, []);

    return (
        <div className="MainPage">
            <BrowserRouter>
                <Header/>
                <div className="content-block">
                    <Switch>
                        <Route path="/" exact component={Quotes}/>
                        <Route path="/quotes" exact component={Quotes}/>
                        <Route path="/quotes/:id/edit"
                               render={props => (
                                   <EditQuote quoteCategories={quoteCategories} {...props}/>)
                               }/>
                        <Route path="/quotes/:id" component={Quotes}/>
                        <Route path="/add-quote"
                               render={props => (
                                   <AddQuote quoteCategories={quoteCategories} {...props}/>)
                               }
                        />
                        <Route render={() => <h1>Not Found</h1>}/>
                    </Switch>
                </div>
            </BrowserRouter>

        </div>
    );
};

export default MainPage;