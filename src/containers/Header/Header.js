import React from 'react';
import './Header.css';
import {NavLink} from "react-router-dom";

const Header = () => {
    return (
        <header className="Header">
            <h1 className="Header__title">Quotes Central</h1>
            <nav className="Header__main-nav main-nav">
                <ul className="main-nav__list">
                    <li className="main-nav__list-item">
                        <NavLink className="main-nav__list-item-link" exact to="/">Quotes</NavLink>
                    </li>
                    <li className="main-nav__list-item">
                        <NavLink className="main-nav__list-item-link" to="/add-quote">Submit new quote</NavLink>
                    </li>
                </ul>
            </nav>
        </header>
    );
};

export default Header;